/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.1.73-community : Database - rms
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rms` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `rms`;

/*Table structure for table `order_info` */

DROP TABLE IF EXISTS `order_info`;

CREATE TABLE `order_info` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(150) DEFAULT NULL,
  `mobile` varchar(150) DEFAULT NULL,
  `address` varchar(600) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `order_info` */

insert  into `order_info`(`order_id`,`city_name`,`mobile`,`address`,`create_date`) values (1,'杭州','13711111111','杭州XXX大街11号','2015-05-05 15:23:07'),(2,'杭州','13722222222','杭州XXX大街12号','2015-05-05 15:23:09'),(3,'北京','13712345678','杭州XXX大街33号','2015-05-05 15:23:20'),(9,'12','22','32','2015-05-26 14:00:22');

/*Table structure for table `r_data_obj` */

DROP TABLE IF EXISTS `r_data_obj`;

CREATE TABLE `r_data_obj` (
  `do_id` int(11) NOT NULL AUTO_INCREMENT,
  `dt_id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`do_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_data_obj` */

/*Table structure for table `r_data_permission` */

DROP TABLE IF EXISTS `r_data_permission`;

CREATE TABLE `r_data_permission` (
  `dp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sr_id` int(11) NOT NULL,
  `expression_type` tinyint(4) NOT NULL,
  `column` varchar(50) NOT NULL,
  `equal` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `r_data_permission` */

insert  into `r_data_permission`(`dp_id`,`sr_id`,`expression_type`,`column`,`equal`,`value`,`remark`) values (1,19,1,'city_name','=','杭州','杭州售后只能查看杭州地区的订单'),(25,19,1,'city_name','=','上海','上海售后只能查看上海地区订单');

/*Table structure for table `r_data_permission_role` */

DROP TABLE IF EXISTS `r_data_permission_role`;

CREATE TABLE `r_data_permission_role` (
  `dp_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`dp_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_data_permission_role` */

insert  into `r_data_permission_role`(`dp_id`,`role_id`) values (1,5),(25,15);

/*Table structure for table `r_data_type` */

DROP TABLE IF EXISTS `r_data_type`;

CREATE TABLE `r_data_type` (
  `dt_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) NOT NULL,
  PRIMARY KEY (`dt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_data_type` */

/*Table structure for table `r_datatype_res` */

DROP TABLE IF EXISTS `r_datatype_res`;

CREATE TABLE `r_datatype_res` (
  `dt_id` int(11) NOT NULL,
  `sr_id` int(11) NOT NULL,
  PRIMARY KEY (`dt_id`,`sr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_datatype_res` */

/*Table structure for table `r_role` */

DROP TABLE IF EXISTS `r_role`;

CREATE TABLE `r_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Data for the table `r_role` */

insert  into `r_role`(`role_id`,`role_name`) values (2,'运营人员'),(3,'产品经理'),(5,'杭州售后'),(6,'超级管理员'),(7,'大区经理'),(8,'客服人员'),(9,'百度人员'),(10,'腾讯人员'),(11,'阿里人员'),(12,'BAT'),(13,'百度贴吧管理员'),(14,'销售主管'),(15,'上海售后'),(16,'广州售后'),(22,'售后');

/*Table structure for table `r_role_permission` */

DROP TABLE IF EXISTS `r_role_permission`;

CREATE TABLE `r_role_permission` (
  `role_id` int(11) NOT NULL,
  `sf_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`sf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_role_permission` */

insert  into `r_role_permission`(`role_id`,`sf_id`) values (6,3),(6,6),(6,8),(6,20),(6,64),(6,69),(9,10),(9,11),(10,16),(11,13),(11,14),(12,10),(12,11),(12,13),(12,14),(12,16),(12,18),(12,19),(13,10),(14,66),(14,67),(22,66);

/*Table structure for table `r_sys_function` */

DROP TABLE IF EXISTS `r_sys_function`;

CREATE TABLE `r_sys_function` (
  `sf_id` int(11) NOT NULL AUTO_INCREMENT,
  `sr_id` int(11) NOT NULL,
  `operate_code` varchar(20) NOT NULL,
  `func_name` varchar(50) NOT NULL,
  PRIMARY KEY (`sf_id`,`operate_code`,`sr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

/*Data for the table `r_sys_function` */

insert  into `r_sys_function`(`sf_id`,`sr_id`,`operate_code`,`func_name`) values (3,5,'view','查看(view)'),(6,7,'view','查看(view)'),(8,8,'view','查看(view)'),(10,11,'view','查看(view)'),(11,10,'view','查看(view)'),(13,16,'view','查看(view)'),(14,15,'view','查看(view)'),(16,13,'view','查看(view)'),(18,10,'update','修改(update)'),(19,11,'qiaoqiaohua','悄悄话(qiaoqiaohua)'),(20,17,'monitor','后台监控(monitor)'),(33,30,'cancelOrder','废除订单(cancelOrder)'),(53,30,'add','添加(add)'),(56,30,'del','删除(del)'),(58,30,'validate','验证(validate)'),(64,6,'view','查看(view)'),(65,30,'checkOrder','查看订单(checkOrder)'),(66,45,'view','查看(view)'),(67,45,'cancelOrder','废除订单(cancelOrder)'),(68,45,'add','添加(add)'),(69,46,'view','查看(view)');

/*Table structure for table `r_sys_operate` */

DROP TABLE IF EXISTS `r_sys_operate`;

CREATE TABLE `r_sys_operate` (
  `operate_code` varchar(20) NOT NULL,
  `operate_name` varchar(50) NOT NULL,
  PRIMARY KEY (`operate_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_sys_operate` */

insert  into `r_sys_operate`(`operate_code`,`operate_name`) values ('add','添加'),('cancelOrder','废除订单'),('checkOrder','查看订单'),('del','删除'),('delEmail','删除邮件'),('monitor','后台监控'),('qiaoqiaohua','悄悄话'),('sendEmail','发送邮件'),('update','修改'),('validate','验证'),('view','查看');

/*Table structure for table `r_sys_res` */

DROP TABLE IF EXISTS `r_sys_res`;

CREATE TABLE `r_sys_res` (
  `sr_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `res_name` varchar(50) NOT NULL,
  `url` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

/*Data for the table `r_sys_res` */

insert  into `r_sys_res`(`sr_id`,`parent_id`,`res_name`,`url`) values (4,0,'后台管理',''),(5,4,'用户管理','manager/user.jsp'),(6,4,'角色管理','manager/role.jsp'),(7,4,'操作管理','manager/sysOperate.jsp'),(8,4,'菜单管理','manager/sysRes.jsp'),(9,0,'百度菜单',''),(10,9,'百度首页','baidu/index.jsp'),(11,9,'百度贴吧','baidu/tieba.jsp'),(12,0,'腾讯菜单',''),(13,12,'腾讯首页','qq/index.jsp'),(14,0,'阿里菜单',''),(15,14,'阿里首页','alibaba/index.jsp'),(16,14,'淘宝网','alibaba/taobao.jsp'),(17,4,'监控页面','manager/monitor.jsp'),(18,0,'订单管理',''),(30,18,'订单查询','order.jsp'),(44,0,'订单管理',''),(45,44,'订单查询','order.jsp'),(46,4,'增删改查','manager/crud.jsp');

/*Table structure for table `r_user` */

DROP TABLE IF EXISTS `r_user`;

CREATE TABLE `r_user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `add_time` datetime NOT NULL,
  `last_login_date` datetime DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_user` */

insert  into `r_user`(`username`,`password`,`add_time`,`last_login_date`) values ('admin','1000:7377e47393d04c49afc7bd02eeab77fa93e92f847d62316f:ebfe6fde6b64b18ea1ed938b0b66a2937c058d56830a09bc','2014-07-21 15:01:18','2015-05-26 17:09:52'),('sellGuagnzhou','1000:93beb4d399e9f23105b449b65e58978ecc5c20c540a058bf:26484eb4ab6e4daecc82a7048bbea2182491d50e0b0e05d6','2015-05-20 18:00:58','2015-05-20 18:01:14'),('sellLeader','1000:8c929f527a8ee1016a67a141090bae7624e18b57c9e8c525:dc2cd59546b81e817aafd60602df369e015ae29370a24b6d','2015-05-04 14:03:22','2015-05-22 16:55:41'),('sellMan','1000:46be61a8dee037601ad12c22364930aaea7f0b5b22eb5296:5018e4a2261562887de6536665c75427773bf5253242c2cb','2014-12-19 16:12:46','2015-05-20 17:29:25'),('sellShanghai','1000:74eb4b5304d0158f8ee4b204c1635454a34406957565029c:7fecba43d175643e233d51068eea900d2de43d0e108329b3','2015-05-08 17:48:42','2015-05-11 14:17:17'),('shouhou','1000:d29efd2a99551cb0a6adeb06d19a32c96f40834926440cee:e38fd90a0968af87d13c6d89f33a094a1a7671093e5020d1','2015-05-22 16:42:46','2015-05-22 16:45:30'),('user_ali','1000:069a760ed3ab5493e3261f9493038c4d4ec2447235b8b69e:43e9dba3eeaae73a5f094f72662eca3c73e5c07bbb4c9c85','2014-07-29 17:02:51','2014-09-11 17:03:16'),('user_baidu','1000:21e8d2013ab3777086ea804673804a21c50d790ccf2fc857:a19f5124721fdeaa63fc210382ef7a18f1dd9596d3f71fad','2014-07-29 16:47:21','2015-05-22 16:13:29'),('user_baidu2','1000:66a1968e3be1e87faf4b61680fb3167059574ba040d3a17b:93cf0abe18a3a055cea4aa1a0c8f1602344b0fafc2fca52e','2014-07-30 15:01:25','2014-09-11 17:27:22'),('user_BAT','1000:2ecc01bff75d8e68390e3fd4547a2ad250181221ab796302:63f03efb32ee55b5f7a71ea4dc7e7013373bd452f4a450ae','2014-07-29 17:03:05','2015-05-22 16:13:46'),('user_qq','1000:a52ec23aaa533ae1642da3186a4687e70eb80afd4584f30c:d90e33a3f60f31f3b2eb06165eb7cd544b0bdbc397bc2d09','2014-07-29 17:02:41','2014-07-29 17:21:58');

/*Table structure for table `r_user_role` */

DROP TABLE IF EXISTS `r_user_role`;

CREATE TABLE `r_user_role` (
  `username` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`username`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_user_role` */

insert  into `r_user_role`(`username`,`role_id`) values ('admin',6),('Lucy',9),('sellGuagnzhou',16),('sellLeader',14),('sellMan',5),('sellShanghai',15),('shouhou',22),('Toom',3),('user_ali',11),('user_baidu',9),('user_baidu2',13),('user_BAT',12),('user_qq',10);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
