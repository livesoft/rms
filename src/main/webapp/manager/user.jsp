<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户管理</title>
</head>
<body>
	
	<div class="easyui-panel search-panel">
		<form id="schForm">
			<table>
				<tr>
					<td class="fm_lab">用户名:</td><td><input name="usernameSch" type="text" class="easyui-validatebox"></td>
					<td>
						<a id="schBtn" class="easyui-linkbutton" iconCls="icon-search">查询</a>
					</td>
				</tr>
			</table>
		</form>
	</div>

     <div id="toolbar">
        <a id="addBtn" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加新用户</a>
    </div>
    <table id="dg"></table>
    
    <div id="dlg" class="easyui-dialog" style="width:420px;height:180px;padding:10px 20px"
            closed="true" modal="true" buttons="#dlg-buttons">
        <form id="fm" method="post">
            <table>
                <tr>
		            <td class="fm_lab">用户名:</td><td><input name="username" type="text" class="easyui-validatebox" required="true"></td>
		        </tr>
                <tr>
		            <td class="fm_lab">密码:</td><td><input id="password" name="password" type="password" class="easyui-validatebox" required="true"></td>
		        </tr>
            </table>
        </form>
    </div>
    <div id="dlg-buttons">
        <a id="saveBtn" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
        <a id="cancelBtn" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
    </div>
    
    <div id="addRoleDlg" class="easyui-dialog" 
    	style="width:500px;height:460px;padding:10px"
    	title="设置角色"
    	data-options="onClose:Globle.clearPanel()"
    	closed="true" modal="true">
    	<div id="roleFrame"></div>
    </div>
    
<jsp:include page="../easyui_lib.jsp"></jsp:include>
<script type="text/javascript" src="${assets}js/MD5.js"></script>
<script type="text/javascript">
(function(){
var that = this;

var pk = 'orderId'; // java类中的主键字段
var listUrl = ctx + 'listRUser.do'; // 查询
var addUrl = ctx + 'addRUser.do'; // 添加
var updateUrl = ctx + 'updateRUser.do'; // 修改
var delUrl = ctx + 'delRUser.do'; // 删除
var submitUrl = ''; // 提交URL

var toolbarId = 'toolbar'; // 工具条ID

var $dialog = $('#dlg'); // 窗口
var $form = $('#fm'); // 表单
var $grid = $('#dg'); // 表格
var $schForm = $('#schForm'); // 查询表单

window.$grid = $grid;

var $schBtn = $('#schBtn'); // 查询按钮
var $saveBtn = $('#saveBtn'); // 保存按钮
var $cancelBtn = $('#cancelBtn'); // 取消按钮
var $addBtn = $('#addBtn'); // 添加按钮

//初始化表格
$grid.datagrid({    
	url:listUrl
   	,columns:[[
   	     {field:'username',title:'用户名',sortable:true}
		, {field:'roles',title:'角色',formatter:roleFormatter}
		, {field:'addTime',title:'添加时间',sortable:true}
		, {field:'lastLoginDate',title:'最后登陆时间',sortable:true}
   	    ,{field:'_btn1',title:'操作',align:'center',formatter:function(val,row){
   	    	 return '<a onclick="'+FunUtil.createFun(that,'resetPassword',row)+' return false;">重置密码</a>'
   	    	 + '<span class="opt-split">|</span>'
   	    	 + '<a onclick="'+FunUtil.createFun(that,'addRole',row)+' return false;">设置角色</a>'
   	    }}
   	]]
   	,toolbar:'#' + toolbarId
	,pagination:true
	,fitColumns:true
	,singleSelect:true
	,striped:true
    ,pageSize:20
});

//-----------初始化事件-----------
$addBtn.click(function(){
	$dialog.dialog('open').dialog('setTitle','添加');
	$form.form('reset');
	submitUrl = addUrl;
});

$saveBtn.click(function(){
	save();
});

$schBtn.click(function(){
	var data = getFormData($schForm);
	$grid.datagrid('load',data);
});

$cancelBtn.click(function(){
	$dialog.dialog('close');
});
//保存
this.save = function(){
	var self = this;
	
	$form.form('submit',{
		url: submitUrl,
		onSubmit: function(){
			var b = $(this).form('validate');
			if(b){
				var $input = $('#password');
				var md5 = faultylabs.MD5($.trim($input.val()))
				$input.val(md5);
			}
			return b;
		},
		success: function(resultTxt){
			var result = $.parseJSON(resultTxt);
			if(result && result.success) {
				$dialog.dialog('close');// close the dialog
				$grid.datagrid('reload');
			}else{
				var errorMsg = result.errorMsg;
				errorMsg = errorMsg + '<br>' + Action.buildValidateError(result);
				MsgUtil.error(errorMsg);
				
				$form.form('reset');
			}
		}
	});
}

this.resetPassword = function(row){
	
	MsgUtil.confirm("确定给"+row.username+"重置密码吗?",function(){
		Action.jsonAsyncActByData(ctx + 'resetUserPassword.do',row,function(e){
			if(e.success){
				MsgUtil.alert('密码重置成功,新密码为:<br><strong style="font-size:14px;color:red;">' + e.msg + '</strong>');
			}
		});
	});
}

this.addRole = function(row){
	var username = row.username;
	$('#roleFrame').html('<iframe src="userRole.jsp?username='+username+'" scrolling="no" frameborder="0" style="width:100%;height:400px;"></iframe>');
	$('#addRoleDlg').dialog('open');
}


})()

function roleFormatter(val,row,index){
	var roles = val;
	if(!roles || roles.length == 0){
		return '<span style="color:red;">未分配角色</span>';
	}
	
	var roleNameHtml = [];
	for(var i=0,len=roles.length; i<len; i++) {
		roleNameHtml.push(roles[i].roleName);
	}
	
	// 所有的角色名
	var roleNameStr = roleNameHtml.join('、');
	
	var resultStr = ['<div title="'+roleNameStr+'">'];
	
	if(roleNameHtml.length > 6){ 
		for(var i=0; i<6; i++) {
			resultStr.push(roleNameHtml[i] + "、");
		}
		resultStr.push('...');
	}else{
		resultStr.push(roleNameStr);
	}
	
	resultStr.push('</div>');
	
	return resultStr.join('');
}

function setRoleCallback(){
	$('#addRoleDlg').dialog('close');
	window.$grid.datagrid('reload');
}

function cancelRoleCallback(){
	$('#addRoleDlg').dialog('close');
}


</script>
</body>
</html>