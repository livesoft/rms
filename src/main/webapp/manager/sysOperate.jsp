<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>操作管理</title>
</head>
<body>

	<div class="easyui-panel search-panel">
		 <form id="schForm">
	        <table>
	        	<tr>
	        		<td class="fm_lab">操作代码:</td><td><input name="operateCodeSch" type="text" class="easyui-validatebox"></td>
	        		<td class="fm_lab">操作名称:</td><td><input name="operateNameSch" type="text" class="easyui-validatebox"></td>
	        		<td><a id="schBtn" class="easyui-linkbutton" iconCls="icon-search">查询</a></td>
	        	</tr>
	        </table>
	    </form>
	</div>

	<div id="toolbar">
	    <a id="addBtn" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加操作类型</a>
	</div>
    
    <table id="dg"></table>
    
    <div id="dlg" class="easyui-dialog" style="width:320px;height:180px;padding:10px 20px"
            closed="true" modal="true" buttons="#dlg-buttons">
        <form id="fm" method="post">
            <table>
                    <tr>
            <td class="fm_lab">操作代码:</td><td><input id="operateCode" name="operateCode" type="text" class="easyui-validatebox" required="true"></td>
        </tr>
                <tr>
            <td class="fm_lab">操作名称:</td><td><input name="operateName" type="text" class="easyui-validatebox" required="true"></td>
        </tr>
            </table>
        </form>
    </div>
    <div id="dlg-buttons">
        <a id="saveBtn" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
        <a id="cancelBtn" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
    </div>
    
    <div id="dlgDel" class="easyui-dialog" style="width:360px;height:280px;padding:10px 20px"
            closed="true" modal="true" title="删除操作" buttons="#dlgdlgDel-buttons">
    	<span style="color:red;">该操作正在被使用,无法删除.</span>
    	<div id="delGrid"></div>
    </div>
    <div id="dlgdlgDel-buttons">
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="$('#dlgDel').dialog('close'); return false;">关闭</a>
    </div>
    
<jsp:include page="../easyui_lib.jsp"></jsp:include>
<script type="text/javascript">

(function(){
var that = this;

var pk = 'operateCode';
var listUrl = ctx + 'listRSysOperate.do';
var addUrl = ctx + 'addRSysOperate.do';
var updateUrl = ctx + 'updateRSysOperate.do';
var delUrl = ctx + 'delRSysOperate.do';
var submitUrl = '';

var toolbarId = 'toolbar';

var $dialog = $('#dlg');
var $form = $('#fm');
var $grid = $('#dg');
var $schForm = $('#schForm');
var $dlgDel = $('#dlgDel');

var $schBtn = $('#schBtn');
var $saveBtn = $('#saveBtn');
var $cancelBtn = $('#cancelBtn');
var $addBtn = $('#addBtn');


$grid.datagrid({    
	url:listUrl
   	,columns:[[
   	      {field:'operateCode',title:'操作代码'}
   	     ,{field:'operateName',title:'操作名称'}
   	     ,{field:'_btn1',title:'操作',align:'center',formatter:function(val,row){
   	    	 return '<a onclick="'+FunUtil.createFun(that,'edit',row)+' return false;">修改</a>'
   	    	 + '<span class="opt-split">|</span>'
   	    	 + '<a onclick="'+FunUtil.createFun(that,'del',row)+' return false;">删除</a>'
   	     }}
   	]]
   	,toolbar:'#' + toolbarId
	,pagination:true
	,fitColumns:true
	,singleSelect:true
	,striped:true
    ,pageSize:20
});

$('#delGrid').datagrid({
	columns:[[    
    	{field:'funcName',title:'使用地方',width:200,formatter:function(val,rowData){
    		return rowData.resName + "-" + val;
    	}}
	]]
	,height:150
	,fitColumns:true
	,striped:true
});

$addBtn.click(function(){
	$dialog.dialog('open').dialog('setTitle','添加');
	$form.form('reset');
	submitUrl = addUrl;
	
	$('#operateCode').prop('disabled',false);
});

$schBtn.click(function(){
	var data = getFormData($schForm);
	$grid.datagrid('load',data);
});

$saveBtn.click(function(){
	save();
});

$cancelBtn.click(function(){
	$dialog.dialog('close');
});


this.edit = function(row){
	if (row){
		$dialog.dialog('open').dialog('setTitle','修改');
		$form.form('clear').form('load',row);
		
		submitUrl = updateUrl + ['?',pk,'=',row[pk]].join('');
		
		// 如果表单中有主键控件,则不能被修改
		$('#operateCode').prop('disabled',true);
	}
}

this.save = function(){
	var self = this;
	$form.form('submit',{
		url: submitUrl,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(resultTxt){
			var result = $.parseJSON(resultTxt);
			Action.execResult(result,function(result){
				$dialog.dialog('close');// close the dialog
				$grid.datagrid('reload');
			});
		}
	});
}

this.del = function(row){
	if(row){
		Action.post(ctx + 'listOperateUse.do',row,function(r){
			if(r.operateCodeUsed){
				var title = '删除[<span style="color:red;">'+row.operateCode+'</span>]'
				$('#delGrid').datagrid('loadData',{rows:r.operateCodeUsedList});
				$dlgDel.dialog('setTitle',title).dialog('open');
			}else{
				$.messager.confirm('确认','确定要删除该数据吗?',function(r){
					if (r){
						Action.post(delUrl,row,function(result){
							Action.execResult(result,function(result){
								$grid.datagrid('reload');
							});
						});
					}
				});
			}
		})
	}
}

})();

/*
var that = this;
var $dlgDel = $('#dlgDel');
var crud = Crud.create({
    pk:'operateCode'
    ,listUrl:ctx + 'listRSysOperate.do'
    ,addUrl:ctx + 'addRSysOperate.do'
    ,updateUrl:ctx + 'updateRSysOperate.do'
    ,delUrl:ctx + 'delRSysOperate.do'
    ,dlgId:'dlg'
    ,formId:'fm'
    ,gridId:'dg'
    ,searchFormId:'schForm'
});

var buttons = [
	{text:'修改',onclick:function(row){
		crud.update(row);
	}}
	,{text:'删除',onclick:function(row){
		del(row);
	}}
];

var appendOpts = {rownumbers:true,pageSize:50};

crud.buildGrid([
 {field:'operateCode',title:'操作代码'}
,{field:'operateName',title:'操作名称'}
,crud.createOperColumn(buttons)
],appendOpts);

$('#delGrid').datagrid({
	columns:[[    
    	{field:'funcName',title:'使用地方',width:200,formatter:function(val,rowData){
    		return rowData.resName + "-" + val;
    	}}
	]]
	,height:150
	,fitColumns:true
	,striped:true
})

function del(row){
	if(row){
		delRow = row;
		Action.post(ctx + 'listOperateUse.do',row,function(r){
			if(r.operateCodeUsed){
				var title = '删除[<span style="color:red;">'+row.operateCode+'</span>]'
				$('#delGrid').datagrid('loadData',r.operateCodeUsedList);
				$('#dlgDel').dialog('setTitle',title).dialog('open');
			}else{
				crud.del(row);
			}
		})
	}
}

function viewOperateUse(){
	$('#delGridCont').show();
}

*/
</script>
</body>
</html>