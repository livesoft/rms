<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>系统资源</title>
<style type="text/css">
.res-item{padding:4px;}
.title_red{color:red;}
</style>
</head>
<body class="easyui-layout">
    <div data-options="region:'west',title:'菜单列表',split:true,border:false" style="width:280px;padding:10px;">
    	<a class="easyui-linkbutton" iconCls="icon-add" onclick="addRootMenu(); return false;">添加根节点</a>
   		<table id="sysResTree"></table>
    </div>   
    <div data-options="region:'center',title:'菜单信息',border:false" style="padding:10px;">
    	<div id="cont_tip"><h3>☜点击菜单查看信息</h3></div>
    	<div id="resInfo" style="display: none;">
    		<div class="res-item">
    			<fieldset>
		    		<legend>基础信息</legend>
		    		 <form id="sysForm" method="post">
		    		 	<input name="parentId" type="hidden">
			            <table>
			                <tr>
					            <td class="fm_lab">菜单名称:</td><td><input name="resName" type="text" class="easyui-validatebox" required="true"></td>
					        </tr>
			                <tr>
					            <td class="fm_lab">url:</td><td><input name="url" type="text" class="easyui-validatebox" style="width: 300px;"></td>
					        </tr>
					        <tr>
					        	<td></td>
					        	<td><a class="easyui-linkbutton" iconCls="icon-ok" onclick="saveRes(); return false;">保存</a></td>
					        </tr>
			            </table>
			        </form>
		    	</fieldset>
    		</div>
			<div class="res-item">
				<fieldset>
		    		<legend>设置操作点</legend>
		    		
		    		<div id="setOptDiv" style="height: 500px;">
		    			<div data-options="region:'center'" style="padding:10px;width: 500px;">
							 <div id="toolbar">
						        <a class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addOpt()">添加操作点</a>
						    </div>
						    <table id="dg"></table>
						</div>
						<div data-options="region:'east',split:true,collapsed:true,title:'操作点列表'" style="width: 550px;">
							<div style="padding:5px;background-color:#fafafa;margin-bottom: 10px;" data-options="iconCls:'icon-search'">
								<form id="optAddfm">
									<table>
										<tr>
											<td class="fm_lab">操作代码:</td><td><input name="operateCodeSch" type="text" class="easyui-validatebox"></td>
											<td class="fm_lab">操作名称:</td><td><input name="operateNameSch" type="text" class="easyui-validatebox"></td>
											<td>
												<a id="searchOptBtn" class="easyui-linkbutton" iconCls="icon-search">查询</a>
											</td>
										</tr>
									</table>
								</form>
							</div>
							<div id="optAddGrid"></div>
						</div>
					</div>
		    	</fieldset>
			</div>
    	</div>
    </div>   
    
    <div id="addRootDl" class="easyui-dialog" style="width:480px;height:180px;padding:10px 20px"
            closed="true" modal="true" buttons="#addRootDldlg-buttons">
        <form id="addRootfm" method="post">
        	<input id="parentId" name="parentId" type="hidden">
            <table>
                <tr>
		            <td class="fm_lab">菜单名称:</td><td><input name="resName" type="text" class="easyui-validatebox" required="true"></td>
		        </tr>
	            <tr>
		            <td class="fm_lab">url:</td><td><input name="url" type="text" class="easyui-validatebox" style="width: 300px;"></td>
		        </tr>
            </table>
        </form>
    </div>
    <div id="addRootDldlg-buttons">
        <a class="easyui-linkbutton" iconCls="icon-ok" onclick="saveRootMenu(); return false;">保存</a>
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="$addRootDl.dialog('close'); return false;">取消</a>
    </div>
        
<jsp:include page="../easyui_lib.jsp"></jsp:include>
<script type="text/javascript">
var that = this;
var selectedNode;
var $contTip = $('#cont_tip');
var $resInfo = $('#resInfo');
var $addRootDl = $('#addRootDl');
var $sysForm = $('#sysForm');
var $addRootfm = $('#addRootfm');
var $setOptDiv = $('#setOptDiv');
var layoutInit = false;

var $sysResTree = $("#sysResTree");
$sysResTree.tree({
    url : ctx + 'listAllMenu.do'
    ,onClick : function (node) {
    	selectedNode = node;
    	var attr = node.attributes;
    	
    	$sysForm.form('load',{parentId:node.parentId,resName:node.resName,url:attr.url});
    	
    	$contTip.hide();
    	$resInfo.show();
    	
    	window.reloadOptCode();
    	window.initLayout();
    }
	,formatter:function(node){
		var text = node.text ;
		text +=buildTreeButton(node);
		return text;
	}
    ,loadFilter:function(menus,parent){
    	for(var i=0,len=menus.length;i<len;i++){
			formatMenu(menus[i]);
		}
    	return menus;
    }
});


function buildTreeButton(node){
	var html = [];
	html.push('<a onclick="'+FunUtil.createFun(window,'addChildNode',node)+' return false;">[添加子节点]</a>');
	if(node.children.length == 0) {
		html.push('<a onclick="'+FunUtil.createFun(window,'delSysRes',node)+' return false;">[删除节点]</a>');
	}
	return html.join('');
}

window.initLayout = function(){
	if(!layoutInit){
		layoutInit = true;
		$setOptDiv.layout(); 
	}
}


function formatMenu(data){
	if(data){
		data.attributes = {url:data.url,srId:data.srId}
	}
	var children = data.children;
	if(children && children.length > 0){
		data.isParent = true;
		for(var i=0,len=children.length;i<len;i++){
			formatMenu(children[i]);
		}
	}
}

function saveRes(){
	var srId = selectedNode ? selectedNode.srId : '';
	var url = ctx + 'updateRSysRes.do?srId=' + srId;
	$sysForm.form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(resultTxt){
			var result = $.parseJSON(resultTxt);
			Action.execResult(result,function(result){
				MsgUtil.topMsg('修改成功');
				$sysResTree.tree('reload');	// reload the user data
			});
		}
	});
}

function delSysRes(node){
	var self = this;
	if (node) {
	var msg = '确定要删除<strong>'+node.resName+'</strong>吗?';
		$.messager.confirm('Confirm',msg,function(r){
			if (r){
				Action.post(ctx + 'delRSysRes.do',{srId:node.srId,resName:node.resName},function(result){
					Action.execResult(result,function(result){
						MsgUtil.topMsg('删除成功');
						$sysResTree.tree('reload');	// reload the user data
						reset();
					});
				});
			}
		});
	}
}

function reset() {
	selectedNode = null;
	$resInfo.hide();
	$contTip.show();
}


function addRootMenu(){
	$('#parentId').val(0);
	this.$addRootDl.dialog('open').dialog('setTitle','添加根节点');
	this.$addRootfm.form('reset');
}

function addChildNode(node){
	$('#parentId').val(node.srId);
	this.$addRootDl.dialog('open').dialog('setTitle','添加子节点');
	this.$addRootfm.form('reset');
}

function saveRootMenu() {
	var url = ctx + 'addRSysRes.do';
	$addRootfm.form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(resultTxt){
			var result = $.parseJSON(resultTxt);
			Action.execResult(result,function(result){
				MsgUtil.topMsg('添加成功');
				this.$addRootDl.dialog('close');
				$sysResTree.tree('reload');	// reload the user data
			});
		}
	});
}

// --------------设置操作点--------------

(function(){
var that = this;

var toolbarId = 'toolbar'; // 工具条ID
var delUrl = ctx + 'delRSysFunction.do';

var $dialog = $('#dlg'); // 窗口
var $form = $('#fm'); // 表单
var $grid = $('#dg'); // 表格
var $schForm = $('#schForm'); // 查询表单

var $schBtn = $('#schBtn'); // 查询按钮
var $saveBtn = $('#saveBtn'); // 保存按钮
var $cancelBtn = $('#cancelBtn'); // 取消按钮
var $addBtn = $('#addBtn'); // 添加按钮

window.SysFunGrid = $grid;
//初始化表格
$grid.datagrid({    
   	columns:[[
   	      {field:'operateCode',title:'操作代码'}
		, {field:'operateName',title:'操作名称'}
   	    ,{field:'_btn1',title:'操作',align:'center',formatter:function(val,row){
   	    	 return '<a onclick="'+FunUtil.createFun(that,'del',row)+' return false;">删除</a>'
   	     }}
   	]]
   	,toolbar:'#' + toolbarId
	,pagination:true
	,fitColumns:true
	,singleSelect:true
	,striped:true
    ,pageSize:20
});

//删除
this.del = function(row){
	if(row){
		$.messager.confirm('确认','确定要删除该数据吗?',function(r){
			if (r){
				Action.post(delUrl,row,function(result){
					Action.execResult(result,function(result){
						$grid.datagrid('reload');
					});
				});
			}
		});
	}
}

window.reloadOptCode = function(){
	if(selectedNode){
		$grid.datagrid({url:ctx + 'listRSysFunction.do?srIdSch='+selectedNode.srId})
	}
}

})()

function addOpt(){
	$setOptDiv.layout('expand','east');
}


// --- 操作代码列表部分,layout east
(function(){
var that = this;

var pk = 'operateCode'; // java类中的主键字段
var listUrl = ctx + 'listRSysOperate.do'; // 查询

var $grid = $('#optAddGrid'); // 表格
var $schForm = $('#optAddfm'); // 查询表单

var $schBtn = $('#searchOptBtn'); // 查询按钮

//初始化表格
$grid.datagrid({    
	url:listUrl
	,rownumbers:true
   	,columns:[[
   	   	 {field:'operateCode',title:'操作代码'}
   		,{field:'operateName',title:'操作名称'}
   	    ,{field:'_btn1',title:'操作',align:'center',formatter:function(val,row){
   	    	 return '<a onclick="'+FunUtil.createFun(that,'doAdd',row)+' return false;">添加</a>'
   	     }}
   	]]
	,pagination:true
	,fitColumns:true
	,singleSelect:true
	,striped:true
    ,pageSize:20
});

$schBtn.click(function(){
	var data = getFormData($schForm);
	$grid.datagrid('load',data);
});

this.doAdd = function(row){
	Action.post(ctx + 'addRSysFunction.do?srId=' + selectedNode.srId,row,function(data){
		Action.execResult(data,function(){
			window.SysFunGrid.datagrid('reload');
		});
	})
}

})()


</script>
</body>
</html>