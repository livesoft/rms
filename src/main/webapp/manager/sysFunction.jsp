<%@page import="org.durcframework.rms.constant.ExpressionTypeConst"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="java.util.Set"%>
<%@page import="org.durcframework.rms.entity.RDataPermission"%>
<%@page import="org.durcframework.rms.entity.RSysOperate"%>
<%@page import="java.util.List"%>
<%@page import="org.durcframework.core.SpringContext"%>
<%@page import="org.durcframework.rms.service.RSysOperateService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>设置操作点</title>
<style type="text/css">
.title{font-size: 12px;padding-bottom: 5px;}
.title_red{color:red;}
</style>
<%
RSysOperateService service = SpringContext.getBean(RSysOperateService.class);
List<RSysOperate> allSysOperates = service.listAllSysOperate();
request.setAttribute("allSysOperates", allSysOperates);
%>
</head>
<body>

	<div id="tt" class="easyui-tabs" data-options="onSelect:tabOnSelectHander"> 
		<!-- tab 设置操作点 -->  
	    <div title="设置操作点" style="padding-top:10px;">   
	        <div class="title">设置<strong class="title_red">${param.resName}</strong>的操作点</div>
			     <div id="toolbar">
			        <a class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()">添加操作点</a>
			    </div>
			    <table id="dg"></table>
			    
			    <div id="dlg" class="easyui-dialog" style="width:450px;height:200px;padding:10px 20px"
			            closed="true" modal="true" buttons="#dlg-buttons">
			        <form id="fm" method="post">
			            <table>
			                <tr>
					            <td class="fm_lab">操作类型:</td>
					            <td>
									<input 
										id="operateCode" 
										name="operateCode"
										class="easyui-combogrid" 
										style="width:200px" 
										data-options="
											panelWidth: 450,
											idField: 'operateCode',
											textField: 'operateName',
											required:true,
											url: '${ctx}listRSysOperate.do',
											pagination:true,
											pageList: [10,20],
											columns: [[
												{field:'operateCode',title:'操作代码',width:200}
												,{field:'operateName',title:'操作名称',width:250}
											]],
											fitColumns: true
										"/>
					            </td>
					        </tr>
			            </table>
			        </form>
			    </div>
			    <div id="dlg-buttons">
			        <a class="easyui-linkbutton" iconCls="icon-ok" onclick="save(); return false;">保存</a>
			        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="crud.closeDlg(); return false;">取消</a>
			    </div> 
	    </div>   
	    <!-- tab 设置数据权限 -->
	    <div title="设置数据权限" style="padding-top:10px;">   
	        <!-- --------DataPermission----------  -->
		    <div id="toolbarDP">
		        <a class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addDP()">添加数据权限</a>
		    </div>
		    
		    <table id="dgDP"></table>
		    
		    <div id="dlgDP" class="easyui-dialog" style="width:650px;height:550px;padding:10px 20px"
		            closed="true" modal="true" buttons="#dlg-buttonsDP">
		        <form id="fmDP" method="post">
		        	<input name="srId" type="hidden" value="${param.srId}">
		            <table>
		                <tr>
				            <td class="fm_lab">条件类型:</td>
				            <td colspan="3">
								<select name="expressionType">
									<option value="<%=ExpressionTypeConst.ValueExpression.getValue() %>">单值条件</option>
								</select>
							</td>
				        </tr>
		                <tr>
		                	<td class="fm_lab">表达式:</td>
				            <td><input name="column" class="easyui-validatebox" required="true"></td>
				            <td>
								<select name="equal">
									<%
										Set<String> equalSet = RDataPermission.EXPRESSION_MAP.keySet();
										String equalText = null;
										for(String equal : equalSet){
											equalText = StringEscapeUtils.escapeHtml(RDataPermission.EXPRESSION_MAP.get(equal));
									%>
									<option value="<%=equal%>"><%=equalText %></option>
									<%} %>
								</select>
							</td>
				            <td><input name="value" class="easyui-validatebox"  required="true"></td>
				        </tr>
		                <tr>
				            <td class="fm_lab">备注:</td>
				            <td colspan="3"><textarea name="remark" class="easyui-validatebox" style="width:400px;height: 40px;"></textarea></td>
				        </tr>
				        <tr>
		                	<td class="fm_lab" valign="top">角色分配:</td>
		                	<td style="width: 300px;" colspan="3">
			                	<table id="dgRoleDP"></table>
		                	</td>
		        		</tr>
		            </table>
		        </form>
		    </div>
		    <div id="dlg-buttonsDP">
		        <a class="easyui-linkbutton" iconCls="icon-ok" onclick="saveDP(); return false;">保存</a>
		        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="crudDP.closeDlg(); return false;">取消</a>
		    </div>  
	    </div>   
	</div>  
   
<jsp:include page="../easyui_lib.jsp"></jsp:include>
<script type="text/javascript" src="${assets}js/plugin/RoleSelect.js"></script>
<script type="text/javascript">
var that = this;
var crud = Crud.create({
    pk:'sfId'
    ,listUrl:ctx + 'listRSysFunction.do?srIdSch=${param.srId}'
    ,addUrl:ctx + 'addRSysFunction.do?srId=${param.srId}'
    ,updateUrl:ctx + 'setSysFunctionRole.do'
    ,delUrl:ctx + 'delRSysFunction.do'
    ,dlgId:'dlg'
    ,formId:'fm'
    ,gridId:'dg'
});

var buttons = [
	{text:'删除',onclick:delPermission}
];

crud.buildGrid([
  {field:'operateCode',title:'操作代码'}
, {field:'operateName',title:'操作名称'}
,crud.createOperColumn(buttons)
],{pageSize:10});

function roleFormatter(val,row,index){
	var roles = row.roles;
	if(!roles || roles.length == 0){
		return '<span style="color:gray;">无</span>';
	}
	
	var roleNameHtml = [];
	
	for(var i=0,len=roles.length; i<len; i++) {
		roleNameHtml.push(roles[i].roleName);
	}
	
	// 所有的角色名
	var roleNameStr = roleNameHtml.join('、');
	
	var resultStr = ['<div title="'+roleNameStr+'">'];
	
	if(roleNameHtml.length > 10){ // 超过10个角色就显示前10个
		for(var i=0; i<10; i++) {
			resultStr.push(roleNameHtml[i] + "、");
		}
		resultStr.push('...');
	}else{
		resultStr.push(roleNameStr);
	}
	
	resultStr.push('</div>');
	
	return resultStr.join('');
}


function add(){
	$('#operateCode').combogrid('enable');
	crud.add('添加<strong class="title_red">${param.resName}</strong>操作点');
	$('#operateCode').combogrid('setValue', 'view');
}

function save(){
	crud.save();
}

function updatePermission(row){
	if (row){
		$('#operateCode').combogrid('disable');
		crud.$dlg.dialog('open').dialog('setTitle','修改<strong class="title_red">${param.resName}</strong>操作点');
		crud.$form.form('clear').form('load',row);
		
		crud.submitUrl = crud.updateUrl + ['?',crud.pk,'=',row[crud.pk]].join('');
		
		// 如果表单中有主键控件,则不能被修改
		if(crud._hasPkInput()){
			crud.getPkInput().prop('disabled',true);
		}
	}
}

function doSelectRole(roles,grid){
	grid.datagrid('clearSelections');
	
	for(var i=0,len=roles.length; i<len; i++) {
		grid.datagrid('selectRecord',roles[i].roleId);
	}
}

function delPermission(row){
	crud.del(row);
}

////////////////////////////////////
var $dgRoleDP;
var inited = false;
var crudDP = Crud.create({
	pk:'dpId'
    ,listUrl:ctx + 'listRDataPermission.do?srIdSch=${param.srId}'
    ,addUrl:ctx + 'addRDataPermission.do?srId=${param.srId}'
    ,updateUrl:ctx + 'updateRDataPermission.do'
    ,delUrl:ctx + 'delRDataPermission.do'
    ,dlgId:'dlgDP'
    ,formId:'fmDP'
    ,gridId:'dgDP'
});

var buttonsDP = [
	{text:'修改',onclick:updateDataPermission}
	,{text:'删除',onclick:delDataPermission}
];

function initGrid(){
	if(!inited){
		inited = true;
		crudDP.buildGrid([
          {field:'roles',title:'角色',formatter:roleFormatter}
        , {field:'expressionTypeName',title:'条件类型'}
        , {field:'expression',title:'表达式'}
        , {field:'remark',title:'备注'}
        ,crud.createOperColumn(buttonsDP)
        ],{toolbar:'#toolbarDP',pageSize:10});
	}
}

function addDP(){
	if(!$dgRoleDP){
		$dgRoleDP = RoleSelect.create('dgRoleDP');
	}
	$dgRoleDP.datagrid('clearSelections');
	crudDP.add('添加<strong class="title_red">${param.resName}</strong>数据权限');
}

function saveDP(){
	
	var rows = $dgRoleDP.datagrid('getSelections');
	if(rows && rows.length > 0){
		crudDP.save();
	}else{
		MsgUtil.topMsg('请选择角色');
	}
}

function updateDataPermission(row){
	if (row){
		crudDP.$dlg.dialog('open').dialog('setTitle','修改<strong class="title_red">${param.resName}</strong>数据权限');
		crudDP.$form.form('clear').form('load',row);
		
		crudDP.submitUrl = crudDP.updateUrl + ['?',crudDP.pk,'=',row[crudDP.pk]].join('');
		
		// 如果表单中有主键控件,则不能被修改
		if(crudDP._hasPkInput()){
			crudDP.getPkInput().prop('disabled',true);
		}
		
		var roles = row.roles;
		if(!$dgRoleDP){
			$dgRoleDP = RoleSelect.create('dgRoleDP',function($dgRoleDP){
				doSelectRole(roles,$dgRoleDP);
			});
		}else{
			doSelectRole(roles,$dgRoleDP);
		}
		
	}
}


function delDataPermission(row){
	crudDP.del(row);
}

function tabOnSelectHander(title,index) {
	if(index == 1){
		initGrid();
	}
}
</script>
</body>
</html>