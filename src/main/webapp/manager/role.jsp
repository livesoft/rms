<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>角色管理</title>
</head>
<body>   
    <div id="toolbar">
        <a id="addBtn" class="easyui-linkbutton" iconCls="icon-add" plain="true">创建新角色</a>
    </div>
    <table id="dg"></table>
    
    <div id="dlg" class="easyui-dialog" style="width:620px;height:400px;padding:10px 20px" 
    	data-options="closed:true,modal:true,maximized:true"
		buttons="#dlg-buttons">
        <form id="fm" method="post">
            <div>
				<div class="div-item">
					<span style="color:red;">*</span>角色名:<input class="easyui-validatebox" type="text" name="roleName" data-options="required:true"/>
				</div>
				<div class="div-item">
					<fieldset>
						<legend>设置权限</legend>
						<div id="sysResTree"></div>
					</fieldset>
				</div>
		    </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a id="saveBtn" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
        <a id="cancelBtn" class="easyui-linkbutton" iconCls="icon-cancel">取消</a>
    </div>
    
    <div id="dlgDel" class="easyui-dialog" style="width:360px;height:280px;padding:10px 20px"
            closed="true" modal="true" title="删除角色" buttons="#dlgdlgDel-buttons">
    	<span style="color:red;">删除后,以下成员将失去该角色及对应的功能.确定删除吗?</span>
    	<div id="delUsernameGrid"></div>
    </div>
    <div id="dlgdlgDel-buttons">
        <a id="doDelBtn" class="easyui-linkbutton" iconCls="icon-ok">确定</a>
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="$('#dlgDel').dialog('close'); return false;">取消</a>
    </div>

    
<jsp:include page="../easyui_lib.jsp"></jsp:include>
<script type="text/javascript">
(function(){
var that = this;

var pk = 'roleId';
var listUrl = ctx + 'listRRole.do'; // 查询
var addUrl = ctx + 'addRRole.do'; // 添加
var updateUrl = ctx + 'updateRRole.do'; // 修改
var delUrl = ctx + 'delRRole.do'; // 修改
var listAllMenuUrl = ctx + 'listAllMenu.do'; // 查询
var submitUrl;

var toolbarId = 'toolbar'; // 工具条ID

var $dialog = $('#dlg'); // 窗口
var $form = $('#fm'); // 表单
var $grid = $('#dg'); // 表格

var $sysResTree = $("#sysResTree");
var $dlgDel = $('#dlgDel');

var $addBtn = $('#addBtn'); // 添加按钮
var $saveBtn = $('#saveBtn'); // 保存按钮
var $cancelBtn = $('#cancelBtn'); // 取消按钮

//初始化表格
$grid.datagrid({    
	url:listUrl
	,rownumbers:true
   	,columns:[[
   	      {field:'roleName',title:'角色名'}
   	     ,{field:'_btn1',title:'操作',align:'center',formatter:function(val,row){
   	    	 return '<a onclick="'+FunUtil.createFun(that,'edit',row)+' return false;">修改</a>'
   	    	 + '<span class="opt-split">|</span>'
   	    	 + '<a onclick="'+FunUtil.createFun(that,'del',row)+' return false;">删除</a>'
   	     }}
   	]]
   	,toolbar:'#' + toolbarId
	,pagination:true
	,fitColumns:true
	,singleSelect:true
	,striped:true
    ,pageSize:20
});

$sysResTree.tree({
    url : listAllMenuUrl
    ,formatter:function(node){
		var text = node.text;
		if(node.sysFuns && node.sysFuns.length > 0){
			text +=buildOperateCheckbox(node.sysFuns)
		}
		return text;
	}
    ,loadFilter:function(menus,parent){
    	for(var i=0,len=menus.length;i<len;i++){
			formatMenu(menus[i]);
		}
    	return menus;
    }
});

$('#delUsernameGrid').datagrid({
	columns:[[    
    	{field:'username',title:'用户名',width:100}  
	]]
	,height:150
	,fitColumns:true
	,striped:true
})

//-----------初始化事件-----------
$addBtn.click(function(){
	$dialog.dialog('open').dialog('setTitle','添加');
	$form.form('reset');
	submitUrl = addUrl;
});

$saveBtn.click(function(){
	save();
});

$cancelBtn.click(function(){
	$dialog.dialog('close');
});
//----------------------


this.edit = function(row) {
	Action.post(ctx + 'listRolePermissionByRoleId.do',row,function(sysFuns){
		var sfIds = [];
		for(var i=0,len=sysFuns.length;i<len;i++){
			sfIds.push(sysFuns[i].sfId);	
		}
		
		$dialog.dialog('open').dialog('setTitle','修改');
		$form.form('clear').form('load',row);
		
		submitUrl = updateUrl + ['?',pk,'=',row[pk]].join('');
		
		$sysResTree.find(':checkbox').val(sfIds);
	});
}

//保存
this.save = function(){
	var self = this;
	$form.form('submit',{
		url: submitUrl,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(resultTxt){
			var result = $.parseJSON(resultTxt);
			Action.execResult(result,function(result){
				$dialog.dialog('close');// close the dialog
				$grid.datagrid('reload');
			});
		}
	});
}

this.del = function(row){
	if (row){
		var userRoles = listRoleUser(row);

		if(userRoles.length > 0){
			var title = '删除[<span style="color:red;">'+row.roleName+'</span>]角色';
			$('#delUsernameGrid').datagrid('loadData',{rows:userRoles});
			$dlgDel.dialog('setTitle',title).dialog('open');
			$('#doDelBtn').unbind().click(function(){
				doDel(row);
			})
		}else{
			MsgUtil.confirm('确定删除该角色吗?',function(){
				doDel(row);
			});
		}
	}
}

function buildOperateCheckbox(sysFuns){
	var html = [];
	var sysFun = null;
	for(var i=0,len=sysFuns.length;i<len;i++){
		sysFun = sysFuns[i];
		html.push('<label style="vertical-align:top;">')
		html.push('<input name="sfId" type="checkbox" style="vertical-align:top;" value="'+sysFun.sfId+'"/>'+sysFun.funcName);
		html.push('</label>');
	}
	
	return html.join('');
}

function formatMenu(data){
	if(data){
		data.attributes = {url:data.url,srId:data.srId}
	}
	var children = data.children;
	if(children && children.length > 0){
		data.isParent = true;
		for(var i=0,len=children.length;i<len;i++){
			formatMenu(children[i]);
		}
	}
}


function doDel(delRow){
	if(delRow){
		Action.post(delUrl,delRow,function(result){
			Action.execResult(result,function(){
				$dlgDel.dialog('close');
				$grid.datagrid('reload');	// reload the user data
			});
		});
	}
}


function listRoleUser(row){
	var ret = [];
	
	Action.postSync(ctx + 'listRoleRelationInfo.do'
			,{roleId:row.roleId},function(result){
		if(result.success){
			ret = result.userRoles;
		}
	});
	
	return ret;
}

})()
</script>
</body>
</html>