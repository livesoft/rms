<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>权限管理系统</title>
<jsp:include page="../easyui_lib.jsp"></jsp:include>
</head>
<body>
<body id="mainLayout" class="easyui-layout">
	<div data-options="region:'north',border:false" style="height:35px;padding:5px;">
		<jsp:include page="header.jsp"></jsp:include>
	</div>
	<div data-options="region:'west',split:true,title:'菜单'" style="width:150px;padding:10px;">
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	<div data-options="region:'center'">
		<div id="mainTab" class="easyui-tabs" data-options="fit:true,border:false">
			<div id="mainTitle" title="首页">
				<jsp:include page="welcome.jsp"></jsp:include>
			</div>
		</div>
	</div>
<!-- 	<div data-options="region:'south',border:false" style="height:25px;"> -->
<%-- 		<jsp:include page="footer.jsp"></jsp:include> --%>
<!-- 	</div> -->

</body>
</html>