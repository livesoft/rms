<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div id="menu"></div>

<script type="text/javascript">
$(function(){
	var mainPage = {    
	    "text":"主页",    
	    "url":"login/welcome.jsp"
	}; 

	var $menu = $("#menu");
	$menu.tree({
	    url : ctx + 'listUserMenu.do',
	    onClick : function (node) {
	    	var attr = node.attributes;
	        if (attr && attr.url) {
	        	openTab(node.text, attr.url + '?srId='+attr.srId);
	        }else{
	        	var target = node.target;
	        	if(node.state == 'open'){
	        		$menu.tree('collapse',target);
	        	}else{
	        		$menu.tree('expand',target);
	        	}
	        }
	        
	    }
	    ,loadFilter:function(menus,parent){
	    	menus.splice(0, 0, mainPage); // 插入主页
	    	for(var i=0,len=menus.length;i<len;i++){
				formatMenu(menus[i]);
			}
	    	return menus;
	    }
	});
	
	function formatMenu(data){
		if(data){
			data.attributes = {url:data.url,srId:data.srId}
		}
		var children = data.children;
		if(children && children.length > 0){
			// 菜单默认收缩
			//data.state = 'closed';
			for(var i=0,len=children.length;i<len;i++){
				formatMenu(children[i]);
			}
		}
	}
	
	var $mainTab = $("#mainTab");
	var firstTab = $mainTab.tabs('getTab',0);
	
	function openTab(text, url) {
    	if(url){
    		$mainTab.tabs('update', {
   	        	tab : firstTab,
   				options : {
   					title:text,
   	            	content : '<iframe src="'+ ctx + url+'" scrolling="yes" frameborder="0" style="width:100%;height:100%;"></iframe>'
   	            }
   			});
    	}
	}
	

});

</script>
