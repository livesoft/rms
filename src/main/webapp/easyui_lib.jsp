<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp" %>
<link href="${ctx}favicon.ico" rel="SHORTCUT ICON">
<link rel="stylesheet" type="text/css" id="easyuiCssId" href="${easyui}themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${easyui}themes/icon.css">
<link rel="stylesheet" type="text/css" href="${easyui}style.css">
<script type="text/javascript">var ctx = '${ctx}';</script>
<script type="text/javascript" src="${assets}js/jquery.min.js"></script>
<script type="text/javascript" src="${easyui}jquery.easyui.min.js"></script>
<script type="text/javascript" src="${easyui}locale/easyui-lang-zh_CN.js"></script>
<c:choose>
	<c:when test="${debugModel}">
		<script type="text/javascript" src="${assets}js/need_compress/Globle.js"></script>
		<script type="text/javascript" src="${assets}js/need_compress/Action.js"></script>
		<script type="text/javascript" src="${assets}js/need_compress/Crud.js"></script>
		<script type="text/javascript" src="${assets}js/need_compress/EventUtil.js"></script>
		<script type="text/javascript" src="${assets}js/need_compress/FunUtil.js"></script>
		<script type="text/javascript" src="${assets}js/need_compress/HtmlUtil.js"></script>
		<script type="text/javascript" src="${assets}js/need_compress/MaskUtil.js"></script>
		<script type="text/javascript" src="${assets}js/need_compress/MsgUtil.js"></script>
	</c:when>
	<c:otherwise>
		<script type="text/javascript" src="${assets}js/common.min.js"></script>
	</c:otherwise>
</c:choose>
<script type="text/javascript">
window.debugModel = ${debugModel};
</script>
