<%@page import="org.durcframework.rms.common.PropertiesManager"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="rms" uri="/rms" %>
<c:set var="ctx" value='<%=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/" %>'/>
<c:set var="assets" value="${ctx}assets/"/>
<c:set var="easyui" value="${assets}easyui/"/>
<c:set var="debugModel" value='<%=PropertiesManager.getInstance().get("debugModel") %>'/>
