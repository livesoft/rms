<%@page import="org.durcframework.rms.util.RightUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>订单管理</title>
</head>
<body>
订单查看页面.
<!-- 方法1,推荐 -->
<rms:role operateCode="cancelOrder">
	<button onclick="alert('废除')">废除订单</button>
</rms:role>

<!-- 方法2 -->
<c:if test='<%=RightUtil.check(request.getParameter("srId"), "cancelOrder") %>'>
	<button onclick="alert('废除2')">废除订单2</button>
</c:if>

    <table id="dg"></table>
    
<jsp:include page="easyui_lib.jsp"></jsp:include>
<script type="text/javascript">
var that = this;
// 能否操作
var couldOperate = <%=RightUtil.check(request.getParameter("srId"), "cancelOrder") %>;
var crud = Crud.create({
    pk:'orderId'
    ,listUrl:ctx + 'listOrderInfo.do?srId='+'${param.srId}'
    ,gridId:'dg'
});

var buttons = [];
if(couldOperate){
	buttons.push({text:'废除订单',onclick:function(){alert('废除')}})
}

crud.buildGrid([
         {field:'cityName',title:'城市'}
 ,        {field:'mobile',title:'手机号'}
 ,        {field:'address',title:'地址'}
 ,        {field:'createDate',title:'创建时间'}
 ,crud.createOperColumn(buttons)
]);
</script>
</body>
</html>