package org.durcframework.rms.controller;

import java.util.List;

import org.durcframework.core.DefaultGridResult;
import org.durcframework.core.UserContext;
import org.durcframework.core.controller.SearchController;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.rms.entity.RSysFunction;
import org.durcframework.rms.entity.RSysRes;
import org.durcframework.rms.entity.RUser;
import org.durcframework.rms.service.RSysFunctionService;
import org.durcframework.rms.service.RSysResService;
import org.durcframework.rms.util.TreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MenuController extends SearchController<RSysRes, RSysResService>{
	
	@Autowired
	private RSysFunctionService sysFunctionService;
	
	/**
	 * 加载用户菜单
	 * @return
	 */
	@RequestMapping("listUserMenu.do")
	public @ResponseBody
	Object listUserMenu(){
		RUser user = UserContext.getInstance().getUser();
		
		if(user == null){
			return error("当前用户不存在");
		}
		
		List<RSysRes> menuList = this.getService().getUserMenu(user.getUsername());
		
		return menuList;
	}
	
    // 获取所有菜单
    @RequestMapping("/listAllMenu.do")
    public @ResponseBody
    Object listAllMenu() {
    	
    	ExpressionQuery query = ExpressionQuery.buildQueryAll();
    	
    	DefaultGridResult resultGrid = (DefaultGridResult)this.queryAll(query);
    	List<RSysRes> rows = (List<RSysRes>)resultGrid.getRows();
    	
    	List<RSysFunction> sysFuns = null;
    	for (RSysRes rSysRes : rows) {
    		sysFuns = sysFunctionService.getBySySResId(rSysRes.getSrId());
			rSysRes.setSysFuns(sysFuns);
		}
    	
		return TreeUtil.buildTreeData(rows);
    }
	

	
}
