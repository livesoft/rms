package org.durcframework.rms.controller;

import java.util.List;
import java.util.Map;

import org.durcframework.core.JsonObjProcessor;
import org.durcframework.core.UserContext;
import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.entity.RUser;
import org.durcframework.rms.entity.RUserSch;
import org.durcframework.rms.service.RRoleService;
import org.durcframework.rms.service.RUserService;
import org.durcframework.rms.util.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RUserController extends
        CrudController<RUser, RUserService> {
	
	@Autowired
	private RRoleService roleService;
	

    @RequestMapping("/addRUser.do")
    public @ResponseBody
	Object addRUser(RUser entity) {
    	String password = entity.getPassword(); // md5加密后的
		password = PasswordUtil.createHash(password);
		entity.setPassword(password);
        return this.save(entity);
    }

    @RequestMapping("/listRUser.do")
    public @ResponseBody
	Object listRUser(RUserSch searchEntity) {
    	return this.queryWithProcessor(searchEntity, new JsonObjProcessor<RUser>() {
			@Override
			public void process(RUser entity, Map<String, Object> jsonObject) {
				List<RRole> userRoles = roleService.getUserRole(entity.getUsername());
				jsonObject.put("roles", userRoles);
			}
		});
    }

    @RequestMapping("/updateRUser.do")
    public @ResponseBody
	Object updateRUser(RUser enity) {
        return this.update(enity);
    }

    @RequestMapping("/delRUser.do")
    public @ResponseBody
	Object delRUser(RUser enity) {
        return this.delete(enity);
    }
    
    @RequestMapping("/resetUserPassword.do")
    public @ResponseBody
	Object resetUserPassword(RUser user){
    	String newPwsd = this.getService().resetUserPassword(user);
    	return success(newPwsd);
    }
    
    @RequestMapping("/updateUserPassword.do")
    public @ResponseBody
	Object updateUserPassword(
    		String oldPswd
    		,String newPswd
    		,String newPswd2
    		){
    	
    	if(!newPswd.equals(newPswd2)){
    		return error("两次输入的新密码不一样");
    	}
    	RUser user = UserContext.getInstance().getUser();
    	RUser storeUser = this.getService().get(user.getUsername());
    	
    	boolean isPswdCorrect = PasswordUtil.validatePassword(oldPswd,
    			storeUser.getPassword());
    	
    	if(!isPswdCorrect){
    		return error("原密码输入有误");
    	}
    	
    	this.getService().updateUserPassword(storeUser,newPswd);
    	
    	return success();
    }
    
}