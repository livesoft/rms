package org.durcframework.rms.controller;

import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.entity.RSysOperate;
import org.durcframework.rms.entity.RSysOperateSch;
import org.durcframework.rms.service.RSysOperateService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RSysOperateController extends
        CrudController<RSysOperate, RSysOperateService> {
	
    @RequestMapping("/addRSysOperate.do")
    public @ResponseBody
	Object addRSysOperate(RSysOperate entity) {
        return this.save(entity);
    }

    @RequestMapping("/listRSysOperate.do")
    public @ResponseBody
	Object listRSysOperate(RSysOperateSch searchEntity) {
        return this.query(searchEntity);
    }

    @RequestMapping("/updateRSysOperate.do")
    public @ResponseBody
	Object updateRSysOperate(RSysOperate enity) {
        return this.update(enity);
    }

    @RequestMapping("/delRSysOperate.do")
    public @ResponseBody
	Object delRSysOperate(RSysOperate enity) {
        return this.delete(enity);
    }
}