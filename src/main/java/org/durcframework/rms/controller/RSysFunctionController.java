package org.durcframework.rms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.durcframework.core.JsonObjProcessor;
import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.entity.AddOperateParam;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.entity.RSysFunction;
import org.durcframework.rms.entity.RSysFunctionSch;
import org.durcframework.rms.entity.RSysOperate;
import org.durcframework.rms.entity.RSysRes;
import org.durcframework.rms.service.AddOperateService;
import org.durcframework.rms.service.RRolePermissionService;
import org.durcframework.rms.service.RRoleService;
import org.durcframework.rms.service.RSysFunctionService;
import org.durcframework.rms.service.RSysOperateService;
import org.durcframework.rms.service.RSysResService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RSysFunctionController extends
        CrudController<RSysFunction, RSysFunctionService> {
	@Autowired
	private RSysOperateService sysOperateService;
	@Autowired
	private RRoleService roleService;
	@Autowired
	private AddOperateService addOperateService;
	@Autowired
	private RSysResService resService;
	@Autowired
	private RRolePermissionService permissionService;
	
	/**
	 * 设置菜单的操作点
	 * @param addOperateParam
	 * @return
	 */
    @RequestMapping("/addRSysFunction.do")
    public @ResponseBody
	Object addRSysFunction(AddOperateParam addOperateParam) {
    	RSysOperate operate = sysOperateService.get(addOperateParam.getOperateCode());
    	if(operate == null){
    		return error("操作类型不存在");
    	}
    	RSysRes res = resService.get(addOperateParam.getSrId());
    	if(res == null){
    		return error("资源不存在");
    	}
    	
    	if(this.getService().isExistSysFun(addOperateParam.getOperateCode(), addOperateParam.getSrId())){
    		return error("操作点已添加");
    	}
    	
		addOperateService.add(res,operate);
		
		return success();
    }
    
    @RequestMapping("/listRSysFunction.do")
    public @ResponseBody
	Object listRSysFunction(RSysFunctionSch searchEntity) {
    	
        return this.queryWithProcessor(searchEntity, new JsonObjProcessor<RSysFunction>() {
			@Override
			public void process(RSysFunction entity,
					Map<String, Object> jsonObject) {
				RSysOperate operate = sysOperateService.get(entity.getOperateCode());
				jsonObject.put("operateName", operate.getOperateName());
				jsonObject.put("operateCode", operate.getOperateCode());
				
				List<RRole> roles = roleService.getRolesBySysFunction(entity);
				jsonObject.put("roles", roles);
			}
		});
    }

    @RequestMapping("/setSysFunctionRole.do")
    public @ResponseBody
	Object setSysFunctionRole(AddOperateParam addOperateParam,int sfId) {
    	this.permissionService.setSysFunctionRole(sfId, addOperateParam.getRoleId());
        return success();
    }

    @RequestMapping("/delRSysFunction.do")
    public @ResponseBody
	Object delRSysFunction(RSysFunction sysFunction) {
        return this.delete(sysFunction);
    }
    
    @RequestMapping("/listOperateUse.do")
    public @ResponseBody
	Object listOperateUse(String operateCode){ 
    	
    	List<RSysFunction> list = this.getService().getByOperateCode(operateCode);
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	boolean operateCodeUsed = CollectionUtils.isNotEmpty(list);
    	
    	if(operateCodeUsed){
    		for (RSysFunction sysFun : list) {
    			RSysRes res = resService.get(sysFun.getSrId());
    			sysFun.setResName(res.getResName());
			}
    	}
    	
    	map.put("operateCodeUsed", operateCodeUsed);
    	map.put("operateCodeUsedList", list);
    	
    	return map;
    }
    
}