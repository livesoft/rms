package org.durcframework.rms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.durcframework.core.MessageResult;
import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.entity.RRoleSch;
import org.durcframework.rms.entity.RUserRole;
import org.durcframework.rms.entity.SetRoleParam;
import org.durcframework.rms.service.RRolePermissionService;
import org.durcframework.rms.service.RRoleService;
import org.durcframework.rms.service.RUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RRoleController extends
        CrudController<RRole, RRoleService> {
	
	@Autowired
	private RUserRoleService userRoleService;
	@Autowired
	private RRolePermissionService rolePermissionService;

    @RequestMapping("/addRRole.do")
    public @ResponseBody
	MessageResult addRRole(SetRoleParam param) {
    	if(StringUtils.hasText(param.getRoleName())){
    		RRole role = new RRole();
    		role.setRoleName(param.getRoleName());		
    		this.getService().addRole(role, param.getSfId());
    		return success();
    	}
    	return error("添加失败");
    }
    
    @RequestMapping("/listRolePermissionByRoleId.do")
    public @ResponseBody Object listRolePermissionByRoleId(int roleId){
    	return rolePermissionService.getRolePermissionByRole(roleId);
    }

    @RequestMapping("/listRRole.do")
    public @ResponseBody
	Object listRRole(RRoleSch searchEntity) {
        return this.query(searchEntity);
    }

    @RequestMapping("/updateRRole.do")
    public @ResponseBody
	Object updateRRole(SetRoleParam param) {
    	if(StringUtils.hasText(param.getRoleName())){
    		RRole role = new RRole();
    		role.setRoleId(param.getRoleId());
    		role.setRoleName(param.getRoleName());		
    		this.getService().updateRole(role, param.getSfId());
    		return success();
    	}
    	return error("修改失败");
    }

    @RequestMapping("/delRRole.do")
    public @ResponseBody
	Object delRRole(RRole enity) {
        return this.delete(enity);
    }
    
    @RequestMapping("/listRoleRelationInfo.do")
    public @ResponseBody
	Object listRoleRelationInfo(int roleId){
    	
    	List<RUserRole> userRoles = userRoleService.getUserRoleByRoleId(roleId);
    	Map<String,Object> retMap = new HashMap<String, Object>();
    	
    	retMap.put("userRoles", userRoles);
    	retMap.put("success", true);
    	
		return retMap;
	}
    
    // ------------
    @RequestMapping("/listAllRRole.do")
    public @ResponseBody
	Object listAllRRole(RRoleSch searchEntity) {
    	return this.queryAll(searchEntity);
    }
  
}