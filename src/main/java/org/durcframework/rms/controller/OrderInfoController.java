package org.durcframework.rms.controller;

import java.util.Date;

import org.durcframework.core.GridResult;
import org.durcframework.core.MessageResult;
import org.durcframework.rms.common.DataPermissionController;
import org.durcframework.rms.entity.OrderInfo;
import org.durcframework.rms.entity.OrderInfoSch;
import org.durcframework.rms.service.OrderInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderInfoController extends
        DataPermissionController<OrderInfo, OrderInfoService> {
	

    @RequestMapping("/addOrderInfo.do")
    public @ResponseBody
	MessageResult addOrderInfo(OrderInfo entity) {
    	entity.setCreateDate(new Date());
        return this.save(entity);
    }

    @RequestMapping("/listOrderInfo.do")
    public @ResponseBody
	GridResult listOrderInfo(OrderInfoSch searchEntity) {
        return this.query(searchEntity);
    }

    @RequestMapping("/updateOrderInfo.do")
    public @ResponseBody
	MessageResult updateOrderInfo(OrderInfo enity) {
        return this.update(enity);
    }

    @RequestMapping("/delOrderInfo.do")
    public @ResponseBody
	MessageResult delDataSource(OrderInfo enity) {
        return this.delete(enity);
    }
    
}