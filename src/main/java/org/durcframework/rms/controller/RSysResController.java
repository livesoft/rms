package org.durcframework.rms.controller;

import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.entity.RSysRes;
import org.durcframework.rms.entity.RSysResSch;
import org.durcframework.rms.service.RSysResService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RSysResController extends
        CrudController<RSysRes, RSysResService> {

    @RequestMapping("/addRSysRes.do")
    public @ResponseBody
	Object addRSysRes(RSysRes entity) {
        return this.save(entity);
    }

    @RequestMapping("/listRSysRes.do")
    public @ResponseBody
	Object listRSysRes(RSysResSch searchEntity) {
        return this.queryAll(searchEntity);
    }
    
    @RequestMapping("/updateRSysRes.do")
    public @ResponseBody
	Object updateRSysRes(RSysRes enity) {
        return this.update(enity);
    }

    @RequestMapping("/delRSysRes.do")
    public @ResponseBody
	Object delRSysRes(RSysRes enity) {
    	if(this.getService().hasChild(enity)){
    		return error(enity.getResName() + "下含有子节点,不能删除.");
    	}
        return this.delete(enity);
    }
    
}