package org.durcframework.rms.entity;

import java.util.List;

import org.durcframework.rms.common.TreeAware;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RSysRes implements TreeAware{
	private int srId;
	private int parentId;
	private String resName;
	private String url;

	private List<RSysRes> children;
	private List<RSysFunction> sysFuns;

	public void setSrId(int srId) {
		this.srId = srId;
	}

	public int getSrId() {
		return this.srId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public int getParentId() {
		return this.parentId;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResName() {
		return this.resName;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return this.url;
	}

	@JsonProperty("_parentId")
	public int getEasyUIParentId() {
		return parentId;
	}

	public List<RSysRes> getChildren() {
		return children;
	}

	public void setChildren(List<RSysRes> children) {
		this.children = children;
	}

	@Override
	public int getId() {
		return this.srId;
	}

	@Override
	public String getText() {
		return resName;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		return this.srId == ((RSysRes)obj).srId;
	}

	public List<RSysFunction> getSysFuns() {
		return sysFuns;
	}

	public void setSysFuns(List<RSysFunction> sysFuns) {
		this.sysFuns = sysFuns;
	}

}