package org.durcframework.rms.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.durcframework.core.expression.annotation.LikeDoubleField;
import org.durcframework.core.expression.annotation.ValueField;
import org.durcframework.core.support.SearchEasyUI;

public class RUserSch extends SearchEasyUI {
	
	private static Map<String, String> sortMap;
	
	static {
		sortMap = new HashMap<String, String>(2);
		// 排序字段映射,java类中的字段对应成数据库中的字段
		sortMap.put("addTime", "add_time");
		sortMap.put("lastLoginDate", "last_login_date");
	}

    private Integer userIdSch;
    private String usernameSch;
    private String passwordSch;
    private Date addTimeSch;
    private Date lastLoginDateSch;
    
    @Override
	protected Map<String, String> getSortMap() {
		return sortMap;
	}

    public void setUserIdSch(Integer userIdSch){
        this.userIdSch = userIdSch;
    }
    
    @ValueField(column = "user_id")
    public Integer getUserIdSch(){
        return this.userIdSch;
    }

    public void setUsernameSch(String usernameSch){
        this.usernameSch = usernameSch;
    }
    
    @LikeDoubleField(column = "username")
    public String getUsernameSch(){
        return this.usernameSch;
    }

    public void setPasswordSch(String passwordSch){
        this.passwordSch = passwordSch;
    }
    
    @ValueField(column = "password")
    public String getPasswordSch(){
        return this.passwordSch;
    }

    public void setAddTimeSch(Date addTimeSch){
        this.addTimeSch = addTimeSch;
    }
    
    @ValueField(column = "add_time")
    public Date getAddTimeSch(){
        return this.addTimeSch;
    }

    public void setLastLoginDateSch(Date lastLoginDateSch){
        this.lastLoginDateSch = lastLoginDateSch;
    }
    
    @ValueField(column = "last_login_date")
    public Date getLastLoginDateSch(){
        return this.lastLoginDateSch;
    }


}