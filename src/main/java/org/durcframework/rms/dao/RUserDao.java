package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RUser;

public interface RUserDao extends BaseDao<RUser> {
}