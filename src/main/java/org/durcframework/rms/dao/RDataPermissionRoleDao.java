package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RDataPermissionRole;

public interface RDataPermissionRoleDao extends BaseDao<RDataPermissionRole> {
}