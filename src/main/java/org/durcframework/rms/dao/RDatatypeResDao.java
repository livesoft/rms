package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RDatatypeRes;

public interface RDatatypeResDao extends BaseDao<RDatatypeRes> {
}