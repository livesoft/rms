package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RDataPermission;

public interface RDataPermissionDao extends BaseDao<RDataPermission> {
}