package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RSysOperate;

public interface RSysOperateDao extends BaseDao<RSysOperate> {
}