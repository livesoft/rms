package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RDataObj;

public interface RDataObjDao extends BaseDao<RDataObj> {
}