package org.durcframework.rms.service;

import org.durcframework.core.service.CrudService;
import org.durcframework.rms.dao.OrderInfoDao;
import org.durcframework.rms.entity.OrderInfo;
import org.springframework.stereotype.Service;

@Service
public class OrderInfoService extends CrudService<OrderInfo, OrderInfoDao> {

}
