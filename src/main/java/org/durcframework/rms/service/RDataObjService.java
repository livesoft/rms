package org.durcframework.rms.service;

import org.durcframework.core.service.CrudService;
import org.durcframework.rms.dao.RDataObjDao;
import org.durcframework.rms.entity.RDataObj;
import org.springframework.stereotype.Service;

@Service
public class RDataObjService extends CrudService<RDataObj, RDataObjDao> {

}
