package org.durcframework.rms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.durcframework.core.DefaultMessageResult;
import org.durcframework.core.expression.Expression;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.expression.subexpression.InnerJoinExpression;
import org.durcframework.core.expression.subexpression.ValueExpression;
import org.durcframework.core.service.CrudService;
import org.durcframework.rms.dao.RRoleDao;
import org.durcframework.rms.entity.RDataPermission;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.entity.RSysFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RRoleService extends CrudService<RRole, RRoleDao> {
	
	@Autowired
	private RUserRoleService userRoleService;
	@Autowired
	private RRolePermissionService permissionService;
	
	public void addRole(RRole role,List<Integer> sfIds) {
		this.save(role);
		
		if(role.getRoleId() > 0) {
			permissionService.setSysFunctionRole(sfIds, role.getRoleId());
		}
	}
	
	public void updateRole(RRole role,List<Integer> sfIds){
		this.update(role);
		
		if(role.getRoleId() > 0) {
			permissionService.setSysFunctionRole(sfIds, role.getRoleId());
		}
	}

	/**
	 * 返回系统功能分配的角色
	 * @param sysFunction
	 * @return
	 */
	public List<RRole> getRolesBySysFunction(RSysFunction sysFunction){
		return this.getDao().findRoleByFunction(sysFunction.getSfId());
	}
	
	/**
	 * 删除角色
	 * 先删除管理表信息,再删除自身
	 */
	@Override
	public void del(RRole entity) {
		userRoleService.delByRoleId(entity.getRoleId());
		permissionService.delByRoleId(entity.getRoleId());
		this.getDao().del(entity);
	}
	
	/**
	 * 获取用户数据权限对应的角色
	 * @param dpId 
	 * @return
	 */
	public List<RRole> getDataPermissionRole(RDataPermission entity){
		ExpressionQuery query = ExpressionQuery.buildQueryAll();
		
		query.add(new InnerJoinExpression("r_data_permission_role", "t2", "role_id", "role_id"));
		
		query.add(new ValueExpression("t2.dp_id", entity.getDpId()));
		
		return this.find(query);
	}
	
	
	/**
	 * 获取用户的角色
	 * @param username
	 * @return
	 */
	public List<RRole> getUserRole(String username){
		List<Expression> expres = new ArrayList<Expression>();
		expres.add(new InnerJoinExpression("r_user_role", "t2", "role_id", "role_id"));
		expres.add(new ValueExpression("t2.username", username));
		
		ExpressionQuery query = ExpressionQuery.buildQueryAll();
		query.addAll(expres);
		
		return this.find(query);
	}
	
}
