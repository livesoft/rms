package org.durcframework.rms.service;

import java.util.List;

import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.service.CrudService;
import org.durcframework.rms.dao.RSysOperateDao;
import org.durcframework.rms.entity.RSysOperate;
import org.springframework.stereotype.Service;

@Service
public class RSysOperateService extends CrudService<RSysOperate, RSysOperateDao> {
	
	public List<RSysOperate> listAllSysOperate(){
		ExpressionQuery query = new ExpressionQuery();
		query.setIsQueryAll(true);
		return this.find(query);
	}
	
}
