package org.durcframework.rms.service;

import org.durcframework.core.service.CrudService;
import org.durcframework.rms.dao.RDatatypeResDao;
import org.durcframework.rms.entity.RDatatypeRes;
import org.springframework.stereotype.Service;

@Service
public class RDatatypeResService extends CrudService<RDatatypeRes, RDatatypeResDao> {

}
