package org.durcframework.rms.service;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.expression.subexpression.ValueExpression;
import org.durcframework.core.service.CrudService;
import org.durcframework.rms.dao.RRolePermissionDao;
import org.durcframework.rms.entity.FunctionRoleParam;
import org.durcframework.rms.entity.RRolePermission;
import org.durcframework.rms.entity.RSysFunction;
import org.springframework.stereotype.Service;

@Service
public class RRolePermissionService extends CrudService<RRolePermission, RRolePermissionDao> {

	/**
	 * 根据功能查询角色权限
	 * @param function
	 * @return
	 */
	public List<RRolePermission> getRolePermissionByFunction(RSysFunction function){
		ExpressionQuery query = new ExpressionQuery();
		query.add(new ValueExpression("sf_id", function.getSfId()));
		query.setIsQueryAll(true);
		
		return this.find(query);
	}
	
	/**
	 * 根据角色查询功能
	 * @param roleId
	 * @return
	 */
	public List<RRolePermission> getRolePermissionByRole(int roleId){
		ExpressionQuery query = new ExpressionQuery();
		query.add(new ValueExpression("role_id", roleId));
		query.setIsQueryAll(true);
		
		return this.find(query);
	}
	
	/**
	 * 批量设置系统功能权限
	 * @param sfId
	 * @param roleIds
	 */
	public void setSysFunctionRole(int sfId,List<Integer> roleIds){
		this.delBySfId(sfId); // 先删除之前的
		
		FunctionRoleParam param = new FunctionRoleParam();
		param.setRoleIds(roleIds);
		param.setSfId(sfId);
		
		this.getDao().setFunctionRole(param);
	}
	
	/**
	 * 设置系统功能权限
	 * @param roleId
	 * @param sfId
	 */
	public void setSysFunctionRole(List<Integer> sfIds,int roleId){
		delByRoleId(roleId); // 删除该角色所有的操作权限
		
		// 添加新的操作权限
		if(CollectionUtils.isNotEmpty(sfIds)){
			RRolePermission rp = null;
			for (Integer sfId : sfIds) {
				rp = new RRolePermission();
				rp.setRoleId(roleId);
				rp.setSfId(sfId);
				
				this.save(rp);
			}
		}
	}
	
	public void delBySfId(int sfId){
		this.getDao().delBySfId(sfId);
	}
	
	public void delByRoleId(int roleId){
		this.getDao().delByRoleId(roleId);
	}
	
}
