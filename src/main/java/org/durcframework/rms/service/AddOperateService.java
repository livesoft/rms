package org.durcframework.rms.service;

import org.durcframework.core.DurcException;
import org.durcframework.rms.dao.RSysFunctionDao;
import org.durcframework.rms.entity.RSysFunction;
import org.durcframework.rms.entity.RSysOperate;
import org.durcframework.rms.entity.RSysRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 添加操作点
 * @author hc.tang
 *
 */
@Service
public class AddOperateService {
	@Autowired
	private RSysFunctionDao functionDao;
	
	/**
	 * 添加操作权限
	 * 1. 添加功能
	 * @param res 资源
	 * @param operate 操作类型
	 */
	public void add(RSysRes res,RSysOperate operate){
		this.addFunction(res,operate);
	}
	
	
	// 添加系统功能,返回保存后的主键值
	private int addFunction(RSysRes res,RSysOperate operate){
		RSysFunction function = new RSysFunction();
		
		function.setOperateCode(operate.getOperateCode());
		function.setSrId(res.getSrId());
		
		function.setFuncName(operate.getOperateName() 
				+ "("+operate.getOperateCode()+")");
		
		
		RSysFunction storeFun = functionDao.get(function);
		
		if(storeFun != null){
			throw new DurcException("添加失败 - [" + function.getFuncName() + "]记录已存在.");
		}
		
		functionDao.save(function);
		
		return function.getSfId();
	}
	
}
